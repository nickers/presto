# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:13-02:00\n"
"PO-Revision-Date: 2009-10-25 21:21+0100\n"
"Last-Translator: Yngve Spjeld Landro <nynorsk@strilen.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. The title of the Applications-tab.
#: TO BE ADDED
msgid "Applications"
msgstr "Applikasjonar"

#. The title of the Account-tab.
#: TO BE ADDED
msgid "Account"
msgstr "Konto"

#. A description of the Home application page and the list that shows the owner's running applications.
#: TO BE ADDED
msgid ""
"This page lists applications running on {username}'s computer. Click for "
"direct access to the content."
msgstr ""
"Denne sida listar applikasjonane som kjører på  {username} si datamaskin.. "
"Klikk for å få direkte tilgang til innhaldet."

#. A description of the message an owner can leave for his visitors.
#: TO BE ADDED
msgid ""
"Leave a message that your friends will see when they visit your Opera Unite "
"applications."
msgstr ""
"Legg igjen ei melding som venene dine kan sjå når dei besøkjer Opera Unite-"
"applikasjonane dine."

#. A submit button for a form to save a personal message for the visitors.
#: TO BE ADDED
msgid "Save"
msgstr "Lagra"

#. A cancel button for a form to cancel editing a personal message for the visitors.
#: TO BE ADDED
msgid "Cancel"
msgstr "Avbryt"

#. A link to the owner's applications
#: TO BE ADDED
msgid "My applications"
msgstr "Applikasjonane mine"

#. A link to the owner's friends' applications
#: TO BE ADDED
msgid "My friends' applications"
msgstr "Venene mine sine applikasjonar"

#. Heading for a list of the owner's running applications.
#: TO BE ADDED
msgid "{username}'s applications"
msgstr "{username} sine applikasjonar"

#. Heading for a list of the owner's friends' running applications.
#: TO BE ADDED
msgid "{username}'s friends' applications"
msgstr "Applikasjonane til {username} sine vener"

#. A message for the owner when no applications are running.
#: TO BE ADDED
msgid ""
"Start using Opera Unite applications to share files, create instant photo "
"galleries, listen to your music from anywhere, and much, much more. You can "
"start these applications from the <A href=\"http://unite.opera.com/support/"
"userguide/#walkthro\">Unite panel</A> and <A href=\"http://unite.opera.com/"
"applications\">download more</A> absolutely free!"
msgstr ""
"Du kan bruka Opera Unite-applikasjonane til å dela filer, raskt laga "
"fotogalleri, lytta til musikken din frå kor som helst og mykje, mykje meir. "
"Du kan starta desse applikasjonane frå <a href=\"http://unite.opera.com/"
"support/userguide/#walkthro\">Unite-panelet</a> og <a href=\"http://unite."
"opera.com/applications\">lasta ned fleire</a> heilt gratis!"

#. A link to launch {serviceName}
#. Eg. 'Launch Media Player', 'Launch File Sharing'
#: TO BE ADDED
msgid "Launch {serviceName}"
msgstr "Start {serviceName}"

#. A message for a visitor when no applications are running.
#: TO BE ADDED
msgid "{username} is not running any applications at the moment."
msgstr "{username} kjører ingen applikasjonar no."

#. An action button that opens an application. It is showed next to each
#. application in the list of applications.
#: TO BE ADDED
msgid "Visit"
msgstr "Besøk"

#. An link button that goes to unite.opera.com/applications/ for the owner
#. to get more applications.
#: TO BE ADDED
msgid "Get More Applications"
msgstr "Hent fleire applikasjonar"

#. A heading for a section that contains the recent activity on all other applications.
#: TO BE ADDED
msgid "Recent activity"
msgstr "Nyleg aktivitet"

#. A link that goes to a page that displays all activity from other applications.
#: TO BE ADDED
msgid "View all"
msgstr "Syn alle"

#. A message that displays activity from the Messenger application.
#. Eg. 'Message received from sjogren in AppName 3 minutes ago.'
#: TO BE ADDED
msgid ""
"Message received from <A href=\"{link}\">{userName}</A> in <A href="
"\"{appLink}\">{appName}</A> {relativeTime}."
msgstr ""
"Motteke melding frå <A href=\"{link}\">{userName}</A> i <A href=\"{appLink}"
"\">{appName}</A> {relativeTime}."

#. A message that displays activity from the File Sharing application.
#. Eg. 'file.mp3 was downloaded from AppName 6 minutes ago.'
#: TO BE ADDED
msgid ""
"<A href=\"{link}\">{filename}</A> was downloaded from <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."
msgstr ""
"<A href=\"{link}\">{filename}</A> blei lasta ned frå <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."

#. A message that displays activity from the Web Server application.
#. Eg. 'index.html was served by AppName 6 minutes ago.'
#: TO BE ADDED
msgid ""
"<A href=\"{link}\">{filename}</A> was served by <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."
msgstr ""
"<A href=\"{link}\">{filename}</A> blei sendt av <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."

#. A message that displays activity from the Photo Sharing application.
#. Eg. 'photo.jpg was viewed in AppName 11 minutes ago.'
#: TO BE ADDED
msgid ""
"<A href=\"{link}\">{filename}</A> was viewed in <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."
msgstr ""
"<A href=\"{link}\">{filename}</A> blei vist i <A href=\"{appLink}\">{appName}"
"</A> {relativeTime}."

#. A message that displays activity from the Photo Sharing application.
#. Eg. 'A slideshow of Summer Vacation 2009 was started in AppName 21 minutes ago.'
#: TO BE ADDED
msgid ""
"A slideshow of <A href=\"{link}\">{albumName}</A> was started in <A href="
"\"{appLink}\">{appName}</A> {relativeTime}."
msgstr ""
"Ei lysbildevising av <A href=\"{link}\">{albumName}</A> blei starta i <A "
"href=\"{appLink}\">{appName}</A> {relativeTime}."

#. A message that displays activity from the Media Player application.
#. Eg. 'Michael Jackson - Beat it was played by AppName 2 minutes ago.'
#: TO BE ADDED
msgid ""
"<A href=\"{link}\">{filename}</A> was played by <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."
msgstr ""
"<A href=\"{link}\">{filename}</A> blei spela av <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."

#. A message that displays activity from the Lounge application.
#. Eg. 'Anders joined AppName 13 minutes ago.'
#: TO BE ADDED
msgid ""
"<A href=\"{link}\">{userName}</A> joined <A href=\"{appLink}\">{appName}</A> "
"{relativeTime}."
msgstr ""
"<A href=\"{link}\">{userName}</A> blei med i <A href=\"{appLink}\">{appName}"
"</A> {relativeTime}."

#. A message that displays activity from the Fridge application.
#. Eg. 'Anders posted a note on AppName 6 minutes ago.'
#: TO BE ADDED
#, fuzzy
msgid ""
"<A href=\"{link}\">{userName}</A> posted a note on <A href=\"{appLink}\">"
"{appName}</A> {relativeTime}."
msgstr ""
"<A href=\"{link}\">{userName}</a> hengde opp ei melding på <A href="
"\"{appLink}\">{appName}</A> {relativeTime}."
