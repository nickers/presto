# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, Kenneth Maage <kennethm@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-08-31 11:17-02:00\n"
"PO-Revision-Date: 2009-10-24 02:34+0800\n"
"Last-Translator: TsungChe (Jack) Wu <sjackwu@yahoo.com>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Column header for the file name
#: templates/fileSharing.html
msgid "Name"
msgstr "檔名"

#. Column header for the file size
#: templates/fileSharing.html
msgid "Size"
msgstr "大小"

#. Column header for the date and time of the file
#: templates/fileSharing.html
msgid "Time"
msgstr "修改日期"

#. Link to save the chosen file
#. Shown immediately to the left of the Size column, slightly gray until hovered
#: templates/fileSharing.html
msgid "Download"
msgstr "下載"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 folder"
msgstr "一個資料夾"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} folders"
msgstr "{counter} 個資料夾"

#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "and"
msgstr "和"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 file"
msgstr "一個檔案"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} files"
msgstr "{counter} 個檔案"

#. Displays instead of the list of files when a folder is empty.
#: templates/fileSharing.html
msgid "This folder is empty."
msgstr "這個資料夾是空的"

#. Error page title text when a resource is not found
#: templates/fileSharing.html
msgid "Folder or file not found"
msgstr "找不到資料夾或檔案"

#. A link that takes the user to the parent folder of a folder.
#. Eg.
#. '<- Parent folder'
#. '      List of files...
#: templates/fileSharing.html
msgid "Parent folder"
msgstr "上一層資料夾"
